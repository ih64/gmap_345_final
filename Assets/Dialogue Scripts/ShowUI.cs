﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowUI : MonoBehaviour
{
    [SerializeField] private Image uiImage;
    public Text dialogue;

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
       // if (other.CompareTag("Player"))
        {
            uiImage.enabled = true;
            dialogue.enabled = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
       // if (other.CompareTag("Player"))
        {
            uiImage.enabled = false;
            dialogue.enabled = false;
        }
    }
}