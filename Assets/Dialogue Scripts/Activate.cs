﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Activate : MonoBehaviour {

    public TextAsset theText;
    public int startLine;
    public int endLine;

    public TextBoxManager theTextManage;

    public bool destroyWhenDone;

	// Use this for initialization
	void Start () {
        theTextManage = FindObjectOfType<TextBoxManager>();
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(waitForPress && Input.GetKeyDown(KeyCode.J))
        {
            theTextManage.ReloadScript(theText);
            theTextManage.currentLine = startLine;
            theTextManage.endAtLine = endLine;
            theTextManage.EnableTextBox();

            if (destroyWhenDone)
            {
                Destroy(gameObject);
            }
        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name == "Player")
        {

            theTextManage.ReloadScript(theText);
            theTextManage.currentLine = startLine;
            theTextManage.endAtLine = endLine;
            theTextManage.EnableTextBox();

            if(destroyWhenDone)
            {
                Destroy(gameObject);
            }
        }
    }
}
