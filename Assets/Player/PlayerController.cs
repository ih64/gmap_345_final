﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public bool canMove;

    void Update()
    {

    if(!canMove)
        {
            return;
        }

        var z = Input.GetAxis("Horizontal") * Time.deltaTime * 50.0f;
        var x = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

        transform.Translate(0, 0, z);
    }
}