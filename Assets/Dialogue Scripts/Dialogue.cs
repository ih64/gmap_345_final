﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{
    // variables
    public Text dialogueText;
    public string message = "";

    void Start()
    {
        dialogueText = GameObject.Find("MessageText").GetComponent<Text>();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            dialogueText.text = message;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            dialogueText.text = "";
        }
    }
}